package man.smoker;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Milutinac on 06-Mar-16.
 */
public class LocationService implements LocationListener {

    private AppCompatActivity activity;
    private GoogleMap mMap;
    private LocationManager locationManager;
    private Location myLocation;
    public static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;
    public static final int PERMISSION_ACCESS_FINE_LOCATION = 2;
    public static final int PERMISSION_ACCESS_BOOTH_LOCATION = 3;

    public LocationService(AppCompatActivity activity, GoogleMap mMap){
        this.activity = activity;
        this.mMap = mMap;
        this.locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        locationManagerSetup();
    }

    @Override
    public void onLocationChanged(Location location) {
        myLocation = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void locationManagerSetup(){
        if ( ContextCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
            if (ContextCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_ACCESS_BOOTH_LOCATION);
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERMISSION_ACCESS_COARSE_LOCATION);
            }
        } else {
            if (ContextCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_ACCESS_FINE_LOCATION);
            } else {
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 5000, 0, this);
                try{
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                            3000, 0, this);
                } catch (Exception e){
                    Toast.makeText(activity, "Greška prilikom GPS praćenja lokacije", Toast.LENGTH_SHORT).show();
                }
                mMap.setMyLocationEnabled(true);
            }
        }
    }

    private void setupCoarseLocation(){
        if ( ContextCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERMISSION_ACCESS_COARSE_LOCATION);
        } else {
            locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 5000, 0, this);
            mMap.setMyLocationEnabled(true);
        }
    }

    private void setupFineLocation(){
        if (ContextCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_ACCESS_FINE_LOCATION);
        } else {
            if (activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS)){
                try{
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                            3000, 0, this);
                    mMap.setMyLocationEnabled(true);
                } catch (Exception e){
                    Toast.makeText(activity, "Greška prilikom GPS praćenja lokacije", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        switch (requestCode) {
            case PERMISSION_ACCESS_COARSE_LOCATION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length == 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setupCoarseLocation();
                }
                break;
            case PERMISSION_ACCESS_FINE_LOCATION:
                if (grantResults.length == 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setupFineLocation();
                }
                break;
            case PERMISSION_ACCESS_BOOTH_LOCATION:
                if (grantResults.length > 1) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                        setupCoarseLocation();
                    }
                    if (grantResults[1] == PackageManager.PERMISSION_GRANTED){
                        setupFineLocation();
                    }
                }
                break;
        }
    }

    public Location getMyLocation(){
        return myLocation;
    }

    public LatLng getMyLocationLatLng(){
        return new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
    }
}
