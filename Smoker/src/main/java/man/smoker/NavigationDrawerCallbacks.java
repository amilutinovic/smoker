package man.smoker;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
