package man.smoker;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;


/**
 * A simple {@link Fragment} subclass.
 */
public class WaitingFragment extends Fragment {

    // public constants
    public static final int HORIZONTAL_PROGRESS_BAR = 1;
    public static final int SMALL_PROGRESS_BAR = 2;
    public static final int NORMAL_PROGRESS_BAR = 3;
    public static final int LARGE_PROGRESS_BAR = 4;

    public static final String BAR_STYLE_NAME = "barStyle";

    // private fields


    public WaitingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        int barType = getArguments().getInt(BAR_STYLE_NAME);

        View v = inflater.inflate(R.layout.fragment_waiting, container, false);
        ProgressBar bar;
        switch (barType){
            case HORIZONTAL_PROGRESS_BAR:
                bar = (ProgressBar) v.findViewById(R.id.horizontalProgressBar);
                break;
            case SMALL_PROGRESS_BAR:
                bar = (ProgressBar) v.findViewById(R.id.smallProgressBar);
                break;
            case NORMAL_PROGRESS_BAR:
                bar = (ProgressBar) v.findViewById(R.id.normalProgressBar);
                break;
            case LARGE_PROGRESS_BAR:
                bar = (ProgressBar) v.findViewById(R.id.largeProgressBar);
                break;
            default:
                bar = (ProgressBar) v.findViewById(R.id.normalProgressBar);
                break;
        }
        bar.setVisibility(View.VISIBLE);
        return v;
    }
}
