package man.smoker;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity
        implements NavigationDrawerCallbacks, OnMapReadyCallback {

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private Toolbar mToolbar;
    private  GoogleMap mMap;
    public static CameraPosition cameraPosition;
    public static float searchRadius;
    public static final int MY_PERMISSION_ACCESS_COURSE_LOCATION = 1;
    private SuggestionsDatabase database;
    private SearchView searchView;
    private Marker lastOpened;
    private AutoCompleteTextView autoCompleteTextView;
    private Location myLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.fragment_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
        // populate the navigation drawer
        mNavigationDrawerFragment.setUserData("John Doe", "johndoe@doe.com", BitmapFactory.decodeResource(getResources(), R.drawable.avatar));
        setUpMapIfNeeded();
        setupAddressSearch();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment =  (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        mMap.setPadding(0, dpToPx(this, 50), 0, 0);
        setActivityListeners();
        CameraUpdate zoom = CameraUpdateFactory.newLatLngZoom(new LatLng(44.8128066, 20.4591467), 13);
        mMap.moveCamera(zoom);
        locationManagerSetup();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            getMenuInflater().inflate(R.menu.main, menu);

            database = new SuggestionsDatabase(this);
            searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.search));
            searchView.setQueryHint(getResources().getString(R.string.search_hint));

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(final String query) {
                    if (query == null) {
                        Toast.makeText(MainActivity.this, "Definiši pojam pretrage", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                    MenuItemCompat.collapseActionView(menu.findItem(R.id.search));
                    mMap.clear();

                    final WaitingFragment waiting = new WaitingFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(WaitingFragment.BAR_STYLE_NAME, WaitingFragment.LARGE_PROGRESS_BAR);
                    waiting.setArguments(bundle);

                    getSupportFragmentManager().beginTransaction().add(R.id.container, waiting).addToBackStack(null).commit();
                    new SearchEngine(MainActivity.this, query, SearchEngine.TOI_SEARCH_TYPE, new SearchEngine.SearchFinishListener() {
                        @Override
                        public void onSearchFinished(int searchType, JSONObject response) {
                            getSupportFragmentManager().beginTransaction().remove(waiting).commit();
                            try {
                                switch (response.getString("s")) {
                                    case "ok":
                                        displayMarkers(response, "toiMarkers", 0, 999);
                                        getSupportActionBar().setTitle(query.trim());
                                        break;
                                    case "er":
                                        // TODO: Proveriti nacin prikaza gresaka prilikom pretrage
                                        Toast.makeText(MainActivity.this, "Greška prilikom pretrage.\nMolimo pokušajte ponovo.", Toast.LENGTH_LONG).show();
                                        break;
                                    case "nf":
                                        Toast.makeText(MainActivity.this, "Traženi pojam nije pronađen u bazi.\nPokušajte ponovo sa drugačijim terminom pretrage", Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                Log.e("MY ERROR", "JSONException: " + e.toString());
                            }
                        }
                    });
                    database.insertSuggestion(query);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    Cursor cursor = database.getSuggestions(newText);
                    if (cursor.getCount() != 0) {
                        String[] columns = new String[]{SuggestionsDatabase.FIELD_SUGGESTION};
                        int[] columnTextId = new int[]{android.R.id.text1};
                        SuggestionSimpleCursorAdapter simple = new SuggestionSimpleCursorAdapter(MainActivity.this, android.R.layout.simple_list_item_2, cursor, columns, columnTextId, 0);
                        searchView.setSuggestionsAdapter(simple);
                        return true;
                    } else {
                        return false;
                    }
                }
            });

            searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
                @Override
                public boolean onSuggestionSelect(int position) {
                    return false;
                }

                @Override
                public boolean onSuggestionClick(int position) {
                    SQLiteCursor cursor = (SQLiteCursor) searchView.getSuggestionsAdapter().getItem(position);
                    int ind = cursor.getColumnIndex(SuggestionsDatabase.FIELD_SUGGESTION);

                    searchView.setQuery(cursor.getString(ind), true);
                    return true;
                }
            });

            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    void displayMarkers(JSONObject json, String extra, int from, int to) throws JSONException {
        JSONArray jsonArray = json.getJSONArray("rr");

        switch (extra) {
            case "toiMarkers":
                to = to > jsonArray.length() ? jsonArray.length() : to;
                Log.d("MARKERS", "to = " + to);
                for (;from < to; from++) {
                    // Create a marker for each city in the JSON data.
                    JSONArray loc = jsonArray.getJSONArray(from);
                    // TODO: Smisliti kako da se markeri obelezavaju, tj da se za svaki marker pamti i id kako bi se posle identifikovao
                    MarkerOptions mo = new MarkerOptions()
                            .title(loc.getString(1))
                            .position(new LatLng(
                                    loc.getJSONArray(2).getDouble(1), // latitude
                                    loc.getJSONArray(2).getDouble(0) // longitude
                            ));
                    mMap.addMarker(mo);
                }
                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                break;
            default:
                Log.d("MARKERS", "zajeb");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    private void setActivityListeners(){
        final ImageButton getDirections = (ImageButton) findViewById(R.id.directions_from_point);
        getDirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MarkerOptions end = new MarkerOptions();
                end.position(lastOpened.getPosition());
                end.title(lastOpened.getTitle());
                Intent intent = new Intent(MainActivity.this, RoutingActivity.class);
                ArrayList<MarkerOptions> mo = new ArrayList<>();
                mo.add(0, end);
                intent.putExtra(RoutingActivity.POINT_ORDER, RoutingActivity.END_START);
                if (getMyLocation() != null){
                    MarkerOptions start = new MarkerOptions();
                    start.position(getMyLocationLatLng());
                    start.title("Moja lokacija");
                    mo.add(1, start);
                }

                intent.putParcelableArrayListExtra(RoutingActivity.SETUP_NAME, mo);
                startActivity(intent);
            }
        });
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            public boolean onMarkerClick(Marker marker) {
                mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
                if (lastOpened != null) {
                    // Close the info window
                    lastOpened.hideInfoWindow();
                    getDirections.setVisibility(View.GONE);

                    if (lastOpened.equals(marker)) {
                        lastOpened = null;
                        getDirections.setVisibility(View.GONE);
                        return true;
                    }
                }
                marker.showInfoWindow();
                lastOpened = marker;
                getDirections.setVisibility(View.VISIBLE);
                return true;
            }
        });
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                getDirections.setVisibility(View.GONE);
            }
        });
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                MainActivity.cameraPosition = cameraPosition;
                setSearchRadius();
            }
        });
    }

    private void setSearchRadius(){
        LatLng farLeft = mMap.getProjection().getVisibleRegion().farLeft;
        LatLng center = mMap.getCameraPosition().target;
        float[] res = new float[1];
        Location.distanceBetween(farLeft.latitude, farLeft.longitude, center.latitude, center.longitude, res);
        MainActivity.searchRadius = res[0];
    }

    private void setupAddressSearch(){
        LinearLayout df = (LinearLayout) findViewById(R.id.dummy_focus);
        // TODO: Dodaj suggested search na osnovu google places
        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.addressSearchTextView);
        autoCompleteTextView.setSelectAllOnFocus(true);
        autoCompleteTextView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (autoCompleteTextView.getText().toString().trim().equals("")){
                        Toast.makeText(MainActivity.this, "Unesi adresu za pretragu...", Toast.LENGTH_SHORT).show();
                    } else {
                        final WaitingFragment waiting = new WaitingFragment();
                        Bundle bundle = new Bundle();
                        bundle.putInt(WaitingFragment.BAR_STYLE_NAME, WaitingFragment.LARGE_PROGRESS_BAR);
                        waiting.setArguments(bundle);

                        InputMethodManager inputManager = (InputMethodManager)
                                MainActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.toggleSoftInput(0, 0);
                        SearchEngine addressSearch = new SearchEngine(MainActivity.this, autoCompleteTextView.getText().toString().trim(), SearchEngine.GEOCODE_SEARCH_TYPE, new SearchEngine.SearchFinishListener() {
                            @Override
                            public void onSearchFinished(int searchType, JSONObject response) {
                                showAddresses(response);
                                getSupportFragmentManager().beginTransaction().remove(waiting).commit();
                            }
                        });
                    }
                }
                return false;
            }
        });
        //autoCompleteTextView.setImeActionLabel("Search", KeyEvent.KEYCODE_ENTER);
        ImageButton searchAddress = (ImageButton) findViewById(R.id.searchForAddress);
        searchAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (autoCompleteTextView.getText().toString().trim().equals("")){
                    Toast.makeText(MainActivity.this, "Unesi adresu za pretragu...", Toast.LENGTH_SHORT).show();
                } else {
                    InputMethodManager inputManager = (InputMethodManager)
                            MainActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.toggleSoftInput(0, 0);

                    final WaitingFragment waiting = new WaitingFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(WaitingFragment.BAR_STYLE_NAME, WaitingFragment.LARGE_PROGRESS_BAR);
                    waiting.setArguments(bundle);

                    SearchEngine addressSearch = new SearchEngine(MainActivity.this, autoCompleteTextView.getText().toString().trim(), SearchEngine.GEOCODE_SEARCH_TYPE, new SearchEngine.SearchFinishListener() {
                        @Override
                        public void onSearchFinished(int searchType, JSONObject response) {
                            showAddresses(response);
                            getSupportFragmentManager().beginTransaction().remove(waiting).commit();
                        }
                    });
                }
            }
        });
    }

    private void showAddresses(JSONObject jsonObj){
        try {
            if (jsonObj.getString("status").equalsIgnoreCase("OK")) {
                JSONArray results = jsonObj.getJSONArray("results");
                if (results.length() == 0){
                    Toast.makeText(MainActivity.this, MainActivity.this.getResources().getString(R.string.address_not_found), Toast.LENGTH_LONG).show();
                }else {
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    for (int i = 0; i < results.length(); i++) {
                        JSONObject object = results.getJSONObject(i);
                        MarkerOptions mo = new MarkerOptions();
                        mo.title(object.getString("formatted_address"));

                        JSONObject location = object.getJSONObject("geometry").getJSONObject("location");
                        LatLng latLng = new LatLng(location.getDouble("lat"), location.getDouble("lng"));
                        mo.position(latLng);
                        mMap.addMarker(mo);
                        builder.include(latLng);
                    }
                    final LatLngBounds latLngBounds = builder.build();
                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 100));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private void locationManagerSetup(){
        if ( ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSION_ACCESS_COURSE_LOCATION);
        } else {
            mMap.setMyLocationEnabled(true);

            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            LocationListener locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    myLocation = location;
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            };

            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 5000, 0,
                    locationListener);

            if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS)){
                /*locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        3000, 0, locationListener);*/
            }
        }
    }

    private Location getMyLocation(){
        return myLocation;
    }

    private LatLng getMyLocationLatLng(){
        if (myLocation != null){
            return new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
        } else {
            return null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_ACCESS_COURSE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationManagerSetup();
                } else {

                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
