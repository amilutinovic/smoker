package man.smoker;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

/**
 * Created by Milutinac on 31-Jan-16.
 */
public class ServerConnection {

    private int urlType;
    private String[] parameters;
    private StringBuilder sb;
    private ResponseListener responseListener;

    public static final int LOGIN_USER = 1;
    public static final int LOCATION_MARKERS = 2;
    public static final int GEOCODE = 3;

    private static final String geocodingKey = "AIzaSyCHjzouI_M-fIC5zDULhO9MAz_7xB7fa4s";
    // DEBUG
    private HashMap<String, Double> times;

    // sadrzi sve url-ove koji se koriste za komunikaciju sa serverom
    // format: http://citymapsearch.com/........
    private static final HashMap<Integer, String> urls;
    static
    {
        urls = new HashMap<>();
        urls.put(LOGIN_USER, "b");
        urls.put(LOCATION_MARKERS, "http://citymapnodejs.westeurope.cloudapp.azure.com/CityAPI");
        //urls.put(LOCATION_MARKERS, "http://10.0.2.2:8080/CityAPI");

    }

    public ServerConnection(int urlType, String... params){
        this.urlType = urlType;
        this.parameters = params;
        if (urlType == LOCATION_MARKERS && (params == null || params.length != 12))
            new Exception("Invalid number of arguments for LTOI Web Service!").printStackTrace();
    }

    public void sendRequest(){
        if (this.responseListener == null) {
            new Exception("ResponseListener can not be null! It must be initialized!").printStackTrace();
        } else {
            RequestData requestData = new RequestData();
            times = new HashMap<>();
            times.put("start", System.currentTimeMillis() / 1000.0);
            requestData.execute();
        }
    }

    private class RequestData extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params)  {
            try {
                if (urlType == GEOCODE){
                    StringBuilder urlStringBuilder = new StringBuilder("https://maps.google.com/maps/api/geocode/json?address=");
                    urlStringBuilder.append(URLEncoder.encode(parameters[0], "utf8"));
                    urlStringBuilder.append("&sensor=false&key=");
                    urlStringBuilder.append(geocodingKey);

                    URL url = new URL(urlStringBuilder.toString());
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);
                    connection.connect();
                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb = sb.append(line + "\n");
                    }
                }else{
                    times.put("b1", System.currentTimeMillis() / 1000.0);
                    Log.d("CONNECTION", "Zapoceto");
                    URL url = new URL(urls.get(urlType));
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    times.put("b11", System.currentTimeMillis() / 1000.0);

                    connection.setReadTimeout(10000);
                    connection.setConnectTimeout(15000);
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);
                    connection.setDoOutput(true);

                    times.put("b12", System.currentTimeMillis() / 1000.0);
                    OutputStream os = new BufferedOutputStream(connection.getOutputStream());

                    times.put("b2", System.currentTimeMillis() / 1000.0);
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(getQueryString());
                    writer.flush();
                    writer.close();
                    os.close();
                    connection.connect();
                    Log.d("RESPONSE CODE", "" + connection.getResponseCode());

                    times.put("b3", System.currentTimeMillis() / 1000.0);
                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb = sb.append(line).append("\n");
                    }
                    times.put("end", System.currentTimeMillis() / 1000.0);
                }

            }catch (Exception e){e.printStackTrace(); }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            try {
                times.put("e1", System.currentTimeMillis() / 1000.0);
                JSONObject response = new JSONObject(sb.toString());
                ServerConnection.this.responseListener.onResponse(response);
                times.put("e2", System.currentTimeMillis() / 1000.0);

                Log.i("TIMESTAMP", "calling execute: " + (times.get("b1") - times.get("start")));
                Log.i("TIMESTAMP", "opening connection: " + (times.get("b11") - times.get("b1")));
                Log.i("TIMESTAMP", "setting parameters: " + (times.get("b12") - times.get("b11")));
                Log.i("TIMESTAMP", "getting output stream: " + (times.get("b2") - times.get("b12")));
                Log.i("TIMESTAMP", "generating query and connecting: " + (times.get("b3") - times.get("b2")));
                Log.i("TIMESTAMP", "getting response: " + (times.get("end") - times.get("b3")));
                Log.i("TIMESTAMP", "calling onPostExecute: " + (times.get("e1") - times.get("end")));
                Log.i("TIMESTAMP", "delivering event: " + (times.get("e2") - times.get("e1")));
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onPostExecute(aVoid);
        }
    }

    private String getQueryString(){
        Uri.Builder builder = new Uri.Builder();
        for (int i = 0; i < parameters.length;){
            builder.appendQueryParameter(parameters[i++], parameters[i++]);
        }
        return builder.build().getEncodedQuery();
    }

    public interface ResponseListener{
        void onResponse(JSONObject response);
    }

    public void setOnResponseListener(ResponseListener resLis){
        this.responseListener = resLis;
    }
}
