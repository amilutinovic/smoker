package man.smoker;

import android.content.Context;
import android.provider.SearchRecentSuggestions;
import android.util.Log;

import com.google.android.gms.maps.model.CameraPosition;

import org.json.JSONObject;

/**
 * Created by Milutinac on 02-Mar-16.
 */
public class SearchEngine {

    // public constants
    public static final int TOI_SEARCH_TYPE = 1;
    public static final int GEOCODE_SEARCH_TYPE = 2;
    public static final int REVERSE_GEOCODE_SEARCH_TYPE = 3;

    // private fields
    private String query;
    private int searchType;
    private Context context;
    private SearchFinishListener searchFinishListener;

    public interface SearchFinishListener{
        public void onSearchFinished(int searchType, JSONObject response);
    }

    public SearchEngine(Context context, String query, int searchType, SearchFinishListener searchFinishListener){
        this.query = query;
        this.searchType = searchType;
        this.context = context;
        this.searchFinishListener = searchFinishListener;

        SearchRecentSuggestions suggestions = new SearchRecentSuggestions(context,
                RecentSearchProvider.AUTHORITY, RecentSearchProvider.MODE);
        suggestions.saveRecentQuery(query, null);

        filterSearch(query);
    }

    private void searchToiPoi(String query){
        CameraPosition cameraPosition = MainActivity.cameraPosition;
        float searchRadius = MainActivity.searchRadius;
        ServerConnection serverConnection = new ServerConnection(ServerConnection.LOCATION_MARKERS, "s", "ltoi", "q", query, "lt", "" + cameraPosition.target.latitude, "lg", "" + cameraPosition.target.longitude, "r", "" + searchRadius, "ru", "m");
        serverConnection.setOnResponseListener(new ServerConnection.ResponseListener() {
            @Override
            public void onResponse(JSONObject response) {
                searchFinishListener.onSearchFinished(searchType, response);
            }
        });
        serverConnection.sendRequest();
    }

    private void filterSearch(String query){
        switch (searchType){
            case TOI_SEARCH_TYPE:
                searchToiPoi(query.trim());
                break;
            case GEOCODE_SEARCH_TYPE:
                searchAddress(query.trim());
                break;
            case REVERSE_GEOCODE_SEARCH_TYPE:
                break;
        }
    }

    private void searchAddress(String address){
        ServerConnection serverConnection = new ServerConnection(ServerConnection.GEOCODE, address);
        serverConnection.setOnResponseListener(new ServerConnection.ResponseListener() {
            @Override
            public void onResponse(JSONObject response) {
                searchFinishListener.onSearchFinished(searchType, response);
            }
        });
        serverConnection.sendRequest();
    }

}
