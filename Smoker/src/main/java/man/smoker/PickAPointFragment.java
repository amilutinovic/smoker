package man.smoker;


import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class PickAPointFragment extends Fragment implements OnMapReadyCallback{

    private AutoCompleteTextView autoCompleteTextView;
    private GoogleMap mMap;
    private Marker marker;
    private PickedPoint mCallBack;
    private MarkerOptions markerOptions;

    public PickAPointFragment() {
        // Required empty public constructor
    }

    public static PickAPointFragment newInstance(MarkerOptions marker, PickedPoint pickedPoint){
        PickAPointFragment pickAPointFragment = new PickAPointFragment();
        pickAPointFragment.mCallBack = pickedPoint;
        pickAPointFragment.markerOptions = marker;
        return pickAPointFragment;
    }

    public interface PickedPoint{
        public void OnPickedPoint(Marker marker);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_pick_apoint, container, false);
        setupAddressSearch(v);

        setUpMapIfNeeded();
        return v;
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment =  (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        mMap.setPadding(0, dpToPx(getActivity(), 50), 0, 0);
        CameraUpdate zoom;
        if (markerOptions != null) {
            marker = mMap.addMarker(markerOptions);
            zoom = CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 17);
        } else {
            zoom = CameraUpdateFactory.newLatLngZoom(new LatLng(44.8128066, 20.4591467), 13);
        }
        mMap.moveCamera(zoom);
        mMap.setMyLocationEnabled(true);
        setActivityListeners();
    }

    private void setActivityListeners(){
        Button choose = (Button) getView().findViewById(R.id.pick_point_btn);
        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallBack.OnPickedPoint(PickAPointFragment.this.marker);
            }
        });
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            public boolean onMarkerClick(Marker marker) {
                mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
                if (PickAPointFragment.this.marker != null) {
                    // Close the info window
                    PickAPointFragment.this.marker.hideInfoWindow();

                    if (PickAPointFragment.this.marker.equals(marker)) {
                        PickAPointFragment.this.marker = null;
                        return true;
                    }
                }
                marker.showInfoWindow();
                PickAPointFragment.this.marker = marker;
                return true;
            }
        });
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.clear();
                marker = mMap.addMarker(new MarkerOptions().position(latLng).title(latLng.toString()));
                mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
            }
        });

        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                LatLng l = new LatLng(mMap.getMyLocation().getLatitude(), mMap.getMyLocation().getLongitude());
                marker = mMap.addMarker(new MarkerOptions().position(l).title(mMap.getMyLocation().toString()));
                return false;
            }
        });
    }

    private void setupAddressSearch(View v){
        LinearLayout df = (LinearLayout) v.findViewById(R.id.dummy_focus);
        // TODO: Dodaj suggested search na osnovu google places
        autoCompleteTextView = (AutoCompleteTextView) v.findViewById(R.id.addressSearchTextView);
        autoCompleteTextView.setSelectAllOnFocus(true);
        autoCompleteTextView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (autoCompleteTextView.getText().toString().trim().equals("")) {
                        Toast.makeText(getActivity(), "Unesi adresu za pretragu...", Toast.LENGTH_SHORT).show();
                    } else {
                        final WaitingFragment waiting = new WaitingFragment();
                        Bundle bundle = new Bundle();
                        bundle.putInt(WaitingFragment.BAR_STYLE_NAME, WaitingFragment.LARGE_PROGRESS_BAR);
                        waiting.setArguments(bundle);

                        InputMethodManager inputManager = (InputMethodManager)
                                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.toggleSoftInput(0, 0);
                        SearchEngine addressSearch = new SearchEngine(getActivity(), autoCompleteTextView.getText().toString().trim(), SearchEngine.GEOCODE_SEARCH_TYPE, new SearchEngine.SearchFinishListener() {
                            @Override
                            public void onSearchFinished(int searchType, JSONObject response) {
                                showAddresses(response);
                                getChildFragmentManager().beginTransaction().remove(waiting).commit();
                            }
                        });
                    }
                }
                return false;
            }
        });
        //autoCompleteTextView.setImeActionLabel("Search", KeyEvent.KEYCODE_ENTER);
        ImageButton searchAddress = (ImageButton) v.findViewById(R.id.searchForAddress);
        searchAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (autoCompleteTextView.getText().toString().trim().equals("")) {
                    Toast.makeText(getActivity(), "Unesi adresu za pretragu...", Toast.LENGTH_SHORT).show();
                } else {
                    InputMethodManager inputManager = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.toggleSoftInput(0, 0);

                    final WaitingFragment waiting = new WaitingFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(WaitingFragment.BAR_STYLE_NAME, WaitingFragment.LARGE_PROGRESS_BAR);
                    waiting.setArguments(bundle);

                    SearchEngine addressSearch = new SearchEngine(getActivity(), autoCompleteTextView.getText().toString().trim(), SearchEngine.GEOCODE_SEARCH_TYPE, new SearchEngine.SearchFinishListener() {
                        @Override
                        public void onSearchFinished(int searchType, JSONObject response) {
                            showAddresses(response);
                            getChildFragmentManager().beginTransaction().remove(waiting).commit();
                        }
                    });
                }
            }
        });
    }

    private void showAddresses(JSONObject jsonObj){
        try {
            if (jsonObj.getString("status").equalsIgnoreCase("OK")) {
                JSONArray results = jsonObj.getJSONArray("results");
                if (results.length() == 0){
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.address_not_found), Toast.LENGTH_LONG).show();
                }else {
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    for (int i = 0; i < results.length(); i++) {
                        JSONObject object = results.getJSONObject(i);
                        MarkerOptions mo = new MarkerOptions();
                        mo.title(object.getString("formatted_address"));

                        JSONObject location = object.getJSONObject("geometry").getJSONObject("location");
                        LatLng latLng = new LatLng(location.getDouble("lat"), location.getDouble("lng"));
                        mo.position(latLng);

                        if (i == 0)
                            marker = mMap.addMarker(mo);
                        else
                            mMap.addMarker(mo);

                        builder.include(latLng);
                    }
                    final LatLngBounds latLngBounds = builder.build();
                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 100));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
