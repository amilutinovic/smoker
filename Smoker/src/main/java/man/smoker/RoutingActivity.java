package man.smoker;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.HashMap;

public class RoutingActivity extends AppCompatActivity implements OnMapReadyCallback, RoutingListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks{

    // public constants
    public static final int SETUP_MARKERS = 0;
    public static final int START_END = 12;
    public static final int END_START = 21;
    public static final String SETUP_NAME = "definedMarkers";
    public static final String POINT_ORDER = "pointOrder";

    // private fields
    private final int START_POINT = 0;
    private final int END_POINT = 1;
    private MarkerOptions start, end;
    private boolean sPoint = false;
    private boolean ePoint = false;
    private AutoCompleteTextView startPoint;
    private AutoCompleteTextView endPoint;
    private Toolbar mToolbar;
    private HashMap<Integer, MarkerOptions> markers;
    private GoogleMap mMap;
    private ArrayList<Polyline> polylines;
    protected GoogleApiClient mGoogleApiClient;
    private int[] colors = new int[]{R.color.primary_dark,R.color.primary,R.color.primary_light,R.color.accent,R.color.primary_dark_material_light};
    private WaitingFragment waiting;
    private Routing.TravelMode travelMode = Routing.TravelMode.DRIVING;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routing);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        startPoint = (AutoCompleteTextView) findViewById(R.id.start_point_name);
        endPoint = (AutoCompleteTextView) findViewById(R.id.end_point_name);
        setActivityListeners();
        //setUpMapIfNeeded();
        if (savedInstanceState == null){
            processPoints();
            showDirections();
        } else {
            Log.d("ROUTING", "saved instance state is not null");
        }
    }

    private void showDirections(){
        // setUpMapIfNeeded();
        if (sPoint && ePoint) {
            if (mMap == null) {
                SupportMapFragment mapp = SupportMapFragment.newInstance();
                getSupportFragmentManager().beginTransaction().replace(R.id.routing_mapContainer, mapp, "map").commit();
                mapp.getMapAsync(this);
                polylines = new ArrayList<>();
                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addApi(Places.GEO_DATA_API)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .build();
                MapsInitializer.initialize(this);
                mGoogleApiClient.connect();
            } else {
                mMap.clear();
                waiting = new WaitingFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(WaitingFragment.BAR_STYLE_NAME, WaitingFragment.LARGE_PROGRESS_BAR);
                waiting.setArguments(bundle);

                getSupportFragmentManager().beginTransaction().add(R.id.routing_mapContainer, waiting).addToBackStack(null).commit();
                Routing routing = new Routing.Builder()
                        .travelMode(travelMode)
                        .withListener(this)
                        .waypoints(start.getPosition(), end.getPosition())
                        .build();
                routing.execute();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        //mMap.setPadding(0, dpToPx(this, 50), 0, 0);
        mMap.setMyLocationEnabled(true);
        showDirections();
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        getSupportFragmentManager().beginTransaction().remove(waiting).commit();
        Toast.makeText(this, "Ruta nije pronađena...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex)
    {
        if(polylines.size()>0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }
        LatLngBounds.Builder builder = LatLngBounds.builder();
        polylines = new ArrayList<>();
        //add route(s) to the map.
        for (int i = 0; i <route.size(); i++) {

            //In case of more than 5 alternative routes
            int colorIndex = i % colors.length;

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(colors[colorIndex]));
            polyOptions.width(10 + i * 3);
            polyOptions.addAll(route.get(i).getPoints());
            Polyline polyline = mMap.addPolyline(polyOptions);
            polylines.add(polyline);
            for (LatLng l : polyline.getPoints()){
                builder.include(l);
            }
        }

        LatLngBounds bounds = builder.build();

        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));

        start.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        mMap.addMarker(start);

        // End marker
        end.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        mMap.addMarker(end);
        getSupportFragmentManager().beginTransaction().remove(waiting).commit();
    }

    @Override
    public void onRoutingCancelled() {
        getSupportFragmentManager().beginTransaction().remove(waiting).commit();
        Toast.makeText(this, "Prekinuto traženje rute...", Toast.LENGTH_SHORT).show();
    }

    private void setActivityListeners(){

        // AutoCompleteTextView Listeners
        startPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPoint(0);
            }
        });
        endPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPoint(1);
            }
        });

        // MY LOCATION LISTENERS
        View.OnClickListener uRazvoju = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(RoutingActivity.this, "U razvoju...", Toast.LENGTH_SHORT).show();
            }
        };
        ImageButton myStart = (ImageButton) findViewById(R.id.my_location_start);
        ImageButton myEnd = (ImageButton) findViewById(R.id.my_location_end);
        myStart.setOnClickListener(uRazvoju);
        myEnd.setOnClickListener(uRazvoju);

        // DIRECTION TYPE LISTENERS
        ImageButton walk = (ImageButton) findViewById(R.id.directions_walk);
        ImageButton bicycle = (ImageButton) findViewById(R.id.directions_bicycle);
        ImageButton car = (ImageButton) findViewById(R.id.directions_car);
        walk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                travelMode = Routing.TravelMode.WALKING;
                showDirections();
            }
        });
        bicycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                travelMode = Routing.TravelMode.BIKING;
                showDirections();
            }
        });
        car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                travelMode = Routing.TravelMode.DRIVING;
                showDirections();
            }
        });
    }

    private void getPoint(final int i){
        final String tag = "pick a point";
        PickAPointFragment p = PickAPointFragment.newInstance(markers.get(i), new PickAPointFragment.PickedPoint() {
            @Override
            public void OnPickedPoint(Marker marker) {
                if (i == 0) {
                    start = new MarkerOptions().position(marker.getPosition()).title(marker.getTitle());
                    startPoint.setText(marker.getTitle());
                    sPoint = true;
                } else {
                    end = new MarkerOptions().position(marker.getPosition()).title(marker.getTitle());
                    endPoint.setText(marker.getTitle());
                    ePoint = true;
                }
                getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag(tag)).commit();
                Log.d("PICK A POINT", "Zavrsio se listener");
                showDirections();
            }
        });
        getSupportFragmentManager().beginTransaction().add(R.id.routeContainer, p, tag).addToBackStack(null).commit();
        Log.d("PICK A POINT", "Zavrsio se getPoint");
    }

    private void processPoints(){
        markers = new HashMap<>();
        Intent i = getIntent();
        if (i.getParcelableArrayListExtra(SETUP_NAME) != null){
            ArrayList<MarkerOptions> mo = i.getParcelableArrayListExtra(SETUP_NAME);
            int order = i.getIntExtra(POINT_ORDER, START_END);
            if (order == START_END){
                if (mo.size() > 1){
                    start = mo.get(0);
                    if (start.getTitle() == null)
                        new Exception("Title of Start Point marker must not be null.").printStackTrace();
                    startPoint.setText(start.getTitle());
                    markers.put(START_POINT, start);
                    sPoint = true;

                    end = mo.get(1);
                    if (end.getTitle() == null)
                        new Exception("Title of End Point marker must not be null.").printStackTrace();
                    endPoint.setText(end.getTitle());
                    markers.put(END_POINT, end);
                    ePoint = true;
                } else if (mo.size() == 1){
                    start = mo.get(0);
                    if (start.getTitle() == null)
                        new Exception("Title of Start Point marker must not be null.").printStackTrace();
                    startPoint.setText(start.getTitle());
                    markers.put(START_POINT, start);
                    sPoint = true;
                    ePoint = false;
                } else {
                    sPoint = false;
                    ePoint = false;
                }
            } else {
                if (mo.size() > 1){
                    end = mo.get(0);
                    if (end.getTitle() == null)
                        new Exception("Title of Start Point marker must not be null.").printStackTrace();
                    endPoint.setText(end.getTitle());
                    markers.put(END_POINT, end);
                    ePoint = true;

                    start = mo.get(1);
                    if (start.getTitle() == null)
                        new Exception("Title of End Point marker must not be null.").printStackTrace();
                    startPoint.setText(start.getTitle());
                    markers.put(START_POINT, start);
                    sPoint = true;
                } else if (mo.size() == 1){
                    end = mo.get(0);
                    if (end.getTitle() == null)
                        new Exception("Title of Start Point marker must not be null.").printStackTrace();
                    endPoint.setText(end.getTitle());
                    markers.put(END_POINT, end);
                    ePoint = true;
                    sPoint = false;
                } else {
                    sPoint = false;
                    ePoint = false;
                }
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        getSupportFragmentManager().beginTransaction().remove(waiting).commit();
        Toast.makeText(this, "Internet konekcija prekinuta...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        getSupportFragmentManager().beginTransaction().remove(waiting).commit();
        Toast.makeText(this, "Greška u internet konekciji...", Toast.LENGTH_SHORT).show();
    }
}
